# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CUDA"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CUDA
  "/home/lore/CLionProjects/CUDA_kernel_image_processing/main.cu" "/home/lore/CLionProjects/CUDA_kernel_image_processing/cmake-build-debug/CMakeFiles/CUDA_kernel_image_processing.dir/main.cu.o"
  )
set(CMAKE_CUDA_COMPILER_ID "NVIDIA")

# The include file search paths:
set(CMAKE_CUDA_TARGET_INCLUDE_PATH
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/lore/CLionProjects/CUDA_kernel_image_processing/Image.cpp" "/home/lore/CLionProjects/CUDA_kernel_image_processing/cmake-build-debug/CMakeFiles/CUDA_kernel_image_processing.dir/Image.cpp.o"
  "/home/lore/CLionProjects/CUDA_kernel_image_processing/Kernel.cpp" "/home/lore/CLionProjects/CUDA_kernel_image_processing/cmake-build-debug/CMakeFiles/CUDA_kernel_image_processing.dir/Kernel.cpp.o"
  "/home/lore/CLionProjects/CUDA_kernel_image_processing/Timer.cpp" "/home/lore/CLionProjects/CUDA_kernel_image_processing/cmake-build-debug/CMakeFiles/CUDA_kernel_image_processing.dir/Timer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
