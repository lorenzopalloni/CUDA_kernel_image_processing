\documentclass{beamer}

\mode<presentation> {
\usetheme{Madrid}
}

\beamertemplatenavigationsymbolsempty

\usepackage{pacman} % Allows you to be the best presentator ever :D

\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage[utf8]{inputenc}
\usepackage{float}
\usepackage{subcaption}

\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{mathtools}

\algrenewcommand\algorithmicindent{0.65em}
\algnewcommand\algorithmicforeach{\textbf{for each}}
\algdef{S}[FOR]{ForEach}[1]{\algorithmicforeach\ #1\ \algorithmicdo \textbf{\ in parallel}}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\title[PC-2018/19 Kernel Image Processing]{PC-2018/19 Kernel Image Processing in CUDA/C++}
\subtitle{Parallel Computing Course}
\author{Lorenzo Palloni}
\institute[]{
    Università Degli Studi di Firenze \\
    \medskip
    \textit{lorenzo.palloni@stud.unifi.it }
}
\date{\today}

\begin{document}

\begin{frame}
\titlepage % Print the title page as the first slide
\end{frame}

%----------------------------------------------------------------------------------------
%	PRESENTATION SLIDES
%----------------------------------------------------------------------------------------
%----------------------------------------------------------------------------------------
%   TABLE OF CONTENTES	
%----------------------------------------------------------------------------------------
\begin{frame}
\tableofcontents
\end{frame}
%----------------------------------------------------------------------------------------
\section{Introduction}
\begin{frame}
\frametitle{Introduction}
\begin{itemize}
\item In this project we compare \textbf{runtimes} of \textbf{convolution operation} between
\begin{itemize}
\item a \textbf{sequential} implementation (in C++) and
\item some \textbf{parallel} implementations (in CUDA/C++).
\end{itemize}
\item We implemented three parallel versions of convolution:
\begin{itemize}
\item a \textbf{naive} version;
\item an optimized version using the \textbf{tiling technique}; 
\item an optimized version using
\begin{itemize}
\item the \textbf{tiling technique} and
\item \textbf{constant memory} of the CUDA-enabled devices.
\end{itemize}
\end{itemize}
\end{itemize}
\end{frame}
%----------------------------------------------------------------------------------------
\subsection{Mask}
\begin{frame}
\frametitle{Mask}
\begin{definition}[Mask]
Let $M \in \mathbb{R}^{2k+1 \ \times \ 2k+1}$ be a matrix of dimensions $(2k+1) \times (2k+1)$ with
\begin{itemize}
\item with $k \in \mathbb{N}$ and
\item such that
\end{itemize}
\begin{align*}
M_{r, c} = M_{c, r},\ \forall\ r, c \in \{1, \dots, 2k+1\}. 
\end{align*}
M is said to be \textbf{mask} (also called \textbf{kernel matrix} or \textbf{convolution matrix}).
\end{definition}
\end{frame}
%----------------------------------------------------------------------------------------
\subsection{Convolution}
\begin{frame}
\frametitle{Convolution}
\begin{definition}[Convolution]
Let
\begin{itemize}
\item $A \in \mathbb{R}^{h \times w}$ be an input matrix of dimensions $h \times w$;
\item $M \in \mathbb{R}^{2k+1 \ \times \ 2k+1}$ be a mask of dimensions $(2k+1) \times (2k+1)$. 
\end{itemize}
Then applying \textbf{convolution} to $A$ (given $M$) returns an output matrix $C \in \mathbb{R}^{h \times w}$ with the same dimensions of $A$ and with each element defined by:
\begin{align}
C_{i,j} = \sum_{s=-k}^{k}\sum_{t=-k}^{k} A_{i + s, j + t} \cdot M_{k + s + 1, k + t + 1}
\end{align}
where $C_{i, j}$ is the element of the matrix C at indexes $(i, j)$, with $i = \{1, \dots, h\}$ and $j = \{1, \dots, w\}$.
\end{definition}
\end{frame}
%----------------------------------------------------------------------------------------
\begin{frame}
\frametitle{An example of convolution applied to an image}
\hfil\hfil\includegraphics[width=5cm]{figures/2-img.jpg}\newline
\null\hfil\hfil\makebox[5cm]{Original, no filter.}\newline
\vfil
\hfil\hfil\includegraphics[width=5cm]{figures/2-img_GaussianBlur_tiled.jpg}\hfil\hfil
\includegraphics[width=5cm]{figures/2-img_EdgeDetection_tiled.jpg}\newline
\null\hfil\hfil\makebox[5cm]{Gaussian blur filter.}
\hfil\hfil\makebox[5cm]{Edge detection filter.}
\end{frame}
%----------------------------------------------------------------------------------------`
\section{Sequential implementation}
\begin{frame}
\frametitle{Sequential implementation}
\begin{algorithm}[H]
	\caption{Convolution}
    \label{alg:alg1}
    \footnotesize
    \hspace*{\algorithmicindent} \textbf{Input}: image $A$, mask $M$ \\
    \hspace*{\algorithmicindent} \textbf{Output}: image $C$ 
	\begin{algorithmic}[1]
		\State $C \gets A$
		\For{$i = 0$ to $h - 1$}
		\For{$j = 0$ to $w - 1$}
		\For{$s = -k$ to $k$}
		\For{$t = -k$ to $k$}
		\If{$-1 < (i + s) < h$ and $-1 < (j + t) < w$} $\ \ \ \ \ \ \leftarrow$  
        \For{$c = 0$ to $c < nChannels$} 
		\State $C[i][j][c] \gets C[i][j][c] + A[i + s][j + t][c] * M[k + s][k + t]$ $\ \ \ \ \ \ \leftarrow$ 
        \EndFor
		\EndIf
		\EndFor
		\EndFor
		\EndFor
		\EndFor
		\State \Return{$C$}
	\end{algorithmic}
\end{algorithm}
\end{frame}
%----------------------------------------------------------------------------------------
\section{Parallel implementations}
%\begin{frame}
%\frametitle{Parallel implementations}
%\begin{itemize}
%Cuda kernels
%\end{itemiV
%\end{frame}
%%----------------------------------------------------------------------------------------
\subsection{Naive parallel implementation}
%\begin{frame}
%\frametitle{Naive parallel implementation (1/2)}
%\begin{algorithm}[H]
%	\caption{ConvolutionParallelStub}
%    \label{alg:alg2}
%    \hspace*{\algorithmicindent} \textbf{Input}: image $A$, mask $M$, tuple \texttt{gridDim}, tuple \texttt{blockDim} \\
%    \hspace*{\algorithmicindent} \textbf{Output}: image $C$ 
%	\begin{algorithmic}[1]
%        \State $C \gets A$
%        \State allocate memory for $A$, $M$ and $C$ in device memory
%        \State copy $A$, $M$ and $C$ from host to device memory
%        \State \textproc{Kernel}{\small\texttt{<<<gridDim, blockDim>>>}}($A$, $M$, $C$)
%        \State copy $C$ from device to host memory
%		\State \Return{C}
%	\end{algorithmic}
%\end{algorithm}
%\begin{itemize}
%\end{itemize}
%\end{frame}
%----------------------------------------------------------------------------------------`
\begin{frame}
\frametitle{Naive parallel implementation}
\begin{algorithm}[H]
	\caption{Kernel}
    \label{alg:alg3}
    \footnotesize
    \hspace*{\algorithmicindent} \textbf{Input}: image $A$, mask $M$, image $C$ \\
    \hspace*{\algorithmicindent} \textbf{Output}: \texttt{void}
	\begin{algorithmic}[1]
        \State i = \texttt{blockIdx.x} * \texttt{blockDim.x} + \texttt{threadIdx.x} $\ \ \ \ \ \ \leftarrow$ 
 
        \State j = \texttt{blockIdx.y} * \texttt{blockDim.y} + \texttt{threadIdx.y} $\ \ \ \ \ \ \leftarrow$ 
 
%		\ForEach{thread identified by $(i, j)$}
        \If{$0 \leq i < w$ and $0 \leq j < h$}
		\For{$s = -k$ to $k$}
		\For{$t = -k$ to $k$}
		\If{$-1 < (i + s) < h$ and $-1 < (j + t) < w$} 
        \For{$c = 0$ to $c < nChannels$}
		\State $C[i][j][c] \gets C[i][j][c] + A[i + s][j + t][c] * M[k + s][k + t]$
        \EndFor
		\EndIf
		\EndFor
		\EndFor
        \EndIf
%		\EndFor
	\end{algorithmic}
\end{algorithm}
\end{frame}
%----------------------------------------------------------------------------------------`
\subsection{Tiling optimization}
\begin{frame}
\frametitle{Tiling optimization}
\begin{itemize}
\item We can imagine an input image "tiled" by \textbf{thread blocks}.
\item Each \textbf{thread} will load a pixel of the image on the shared memory (shared within a thread block).
\item When each pixel computes its contribution to the convolution it load the informations from \textbf{shared memory} instead of from global memory (physically located farther).
\end{itemize}
\end{frame}
%----------------------------------------------------------------------------------------`
\subsection{Constant memory optimization}
\begin{frame}
\frametitle{Constant memory optimization}
\begin{itemize}
\item \textbf{Constant memory} is a limited-size, read-only type of memory.
\item The accesses on constant memory are \textbf{faster} than from global memory.
\item We can exploit this type of memory to store the \textbf{mask}.
\end{itemize}
\end{frame}
%----------------------------------------------------------------------------------------`
\section{Results}
\begin{frame}
\frametitle{Results (1/4)}
\begin{itemize}
\item Runtimes of the implementations are compared in function of the following parameters:
\begin{itemize}
\item four different RGB image with \textbf{resolutions}: \{100$\times$100, 2K, 4K, 8K\};
\item two different \textbf{dimensions of mask}: \{5$\times$5, 9$\times$9\};
\item two different \textbf{dimensions of thread block}: \{16$\times$16, 32$\times$32\}.
\end{itemize}
\item Each experiment was repeated \textbf{30 times} in order to compute average and standard deviation.
\item All experiments have been performed on a GPU NVIDIA 1070 with MAX-Q Design and a CPU i7-8750H.
\end{itemize}

\end{frame}
%----------------------------------------------------------------------------------------`
\begin{frame}
\frametitle{Results (2/4) - Kernel executions only}
\begin{columns}[t]
\column{.5\textwidth}
\centering
\includegraphics[width=6.5cm,height=4cm,keepaspectratio]{figures/NO_TRANSFER/5X5_16X16.png}\\
\includegraphics[width=6.5cm,height=4cm,keepaspectratio]{figures/NO_TRANSFER/5X5_32X32.png}
\column{.5\textwidth}
\centering
\includegraphics[width=6.5cm,height=4cm,keepaspectratio]{figures/NO_TRANSFER/9X9_16X16.png}\\
\includegraphics[width=6.5cm,height=4cm,keepaspectratio]{figures/NO_TRANSFER/9X9_32X32.png}
\end{columns}
\end{frame}
%----------------------------------------------------------------------------------------`
\begin{frame}
\frametitle{Results (3/4) - Taking into account the data transfers too}
\begin{columns}[t]
\column{.5\textwidth}
\centering
\includegraphics[width=6.5cm,height=4cm,keepaspectratio]{figures/WITH_TRANSFER/5X5_16X16.png}\\
\includegraphics[width=6.5cm,height=4cm,keepaspectratio]{figures/WITH_TRANSFER/5X5_32X32.png}
\column{.5\textwidth}
\centering
\includegraphics[width=6.5cm,height=4cm,keepaspectratio]{figures/WITH_TRANSFER/9X9_16X16.png}\\
\includegraphics[width=6.5cm,height=4cm,keepaspectratio]{figures/WITH_TRANSFER/9X9_32X32.png}
\end{columns}

\end{frame}
%----------------------------------------------------------------------------------------`
\begin{frame}
\frametitle{Results (4/4)}
\begin{itemize}
\item Results show the almost ubiquitous order of the runtimes of the kernel implementations (naive $<$ tiling $<$ tiling + constant).
\item The implementations are scalable \textbf{by mitigating the data transfers}.
\end{itemize}
\end{frame}
%----------------------------------------------------------------------------------------`
\section{Conclusions}
\begin{frame}
\frametitle{Conclusions}
\begin{itemize}
\item Among the \textbf{kernel-only executions} we have achieved a maximum average speedup of \textbf{470X} on an 8K image (84X with naive implementation, same context).
\item Among the \textbf{stub function executions} we have achieved a maximum average speedup of \textbf{114X} on a FHD image (15X with naive implementation, same context).
\end{itemize}
\end{frame}
%----------------------------------------------------------------------------------------`
%----------------------------------------------------------------------------------------`
%----------------------------------------------------------------------------------------`
%----------------------------------------------------------------------------------------`

\begingroup
\footnotesize
\begin{frame}
\frametitle{References}

\begin{thebibliography}{99} % Beamer does not support BibTeX so references must be inserted manually as below

\bibitem{pmpp2016}{Kirk, David B., and W. Hwu Wen-Mei. Programming massively parallel processors: a hands-on approach. Morgan kaufmann, 2016.}

\bibitem{cudac}{NVIDIA. CUDA C Programming Guide (PG-02829-001\_v10.1). NVIDIA Corporation, 2019.}

\end{thebibliography}

\end{frame}
\endgroup
%------------------------------------------------
%------------------------------------------------
%------------------------------------------------
%------------------------------------------------
%------------------------------------------------
\begin{frame}
\frametitle{Extra - Tiling implementation (1/3)}
\begin{itemize}
\item In tiled version \textbf{execution configuration parameters} have to be chosen not only with respect to the size of the input \textbf{image dimensions} but also with respect to the \textbf{mask dimensions}.
\item We have to define \texttt{gridDim.y} as
\begin{align*}
\Big\lceil\frac{h + (2 * k)}{\texttt{blockDim.y} - k}\Big\rceil,
\end{align*}
and the \texttt{gridDim.x} as
\begin{align*}
\Big\lceil\frac{w + (2 * k)}{\texttt{blockDim.x} - k}\Big\rceil,
\end{align*}
where:
\begin{itemize}
\item $h$ is the height of the image;
\item $w$ is the width of the image;
\item $k$ is the integer half of the mask length;
\end{itemize}
\end{itemize}
\end{frame}
%------------------------------------------------
\begin{frame}
\frametitle{Extra - Tiling implementation (2/3)}
\begin{itemize}
\item We also have to define a \textbf{dummy thread block} that has the following properties:
\begin{itemize}
\item is smaller and \textbf{central} to the original thread block;
\item has dimensions $(\texttt{blockDim.y} - k)\times(\texttt{blockDim.y} - k)$.
\end{itemize}
\end{itemize}
\end{frame}
%------------------------------------------------
\begin{frame}
\frametitle{Extra - Tiling implementation (3/3)}
\begin{enumerate}
\item Maps each thread within a \textbf{dummy thread block} with the image one-to-one.
\item Allocates a \textbf{shared matrix} of the same dimensions of a thread block in the shared memory. Each thread is mapped to the shared matrix one-to-one.
\item Each thread \textbf{loads} the pixel where it was mapped in the shared matrix in the position where it was mapped in.
\item \textbf{Synchronize} all the threads.
\item Each thread within a dummy thread block \textbf{computes} its own contribute to the convolution operation.
\end{enumerate}
\end{frame}
%------------------------------------------------
\end{document}
