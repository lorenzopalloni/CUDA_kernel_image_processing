\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Mask}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Convolution}{5}{0}{1}
\beamer@sectionintoc {2}{Sequential implementation}{7}{0}{2}
\beamer@sectionintoc {3}{Parallel implementations}{8}{0}{3}
\beamer@subsectionintoc {3}{1}{Naive parallel implementation}{8}{0}{3}
\beamer@subsectionintoc {3}{2}{Tiling optimization}{9}{0}{3}
\beamer@subsectionintoc {3}{3}{Constant memory optimization}{10}{0}{3}
\beamer@sectionintoc {4}{Results}{11}{0}{4}
\beamer@sectionintoc {5}{Conclusions}{15}{0}{5}
