#include "Kernel.h"
#include "Image.h"
#include "Timer.h"
#include <cuda.h>
#include <iostream>
#include <string>
#include <cassert>
#include <iomanip>

#define cudaErrorCheck(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true) {
   if (code != cudaSuccess) {
      fprintf(stderr, "GPU assert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

constexpr unsigned char TILE_WIDTH{32};
constexpr unsigned char MASK_WIDTH{9};
constexpr unsigned char CHANNELS{3};
// 32 * 32 * 3 * 4 == 12288 bytes
// 16 * 16 * 3 * 4 ==  3072 bytes
// sharedMemPerBlock( "GeForce GTX 1070 with Max-Q Design" ) == 49152 bytes
constexpr unsigned char TILE_WIDTH_OUT{TILE_WIDTH - (MASK_WIDTH - 1)};

__constant__ float MASK[MASK_WIDTH * MASK_WIDTH];

__host__ float* convolutionHost(const float *in, const Kernel *kernel, const int w, const int h, const int nChannels);

__global__ void convolutionKernel(const float* __restrict__ in, float *out, const float* __restrict__ mask,
        const int maskWidth, const int w, const int h, const int nChannels);

__host__ float* convolutionKernelStub(const float *h_dataIn, const Kernel *kernel, const int w, const int h,
        const int nChannels, double elapsedSequential);

__global__ void convolutionKernelTiled(const float* __restrict__ in, float *out, const float* __restrict__ mask,
        const int width, const int height, const int width_grid, const int height_grid);

__host__ float* convolutionKernelTiledStub(const float *h_dataIn, const Kernel *kernel, const int w, const int h,
        double elapsedSequential);

__global__ void convolutionKernelTiledConstant(const float* __restrict__ in, float *out, const int width,
        const int height, const int width_grid, const int height_grid);

__host__ float* convolutionKernelTiledConstantStub(const float *h_dataIn, const Kernel *kernel, const int w,
        const int h, double elapsedSequential);

int main() {

    std::string fn = "../data/4-img.ppm";

    // init convolution matrix
//    Kernel *kernel = new GaussianBlur(2, MASK_WIDTH);
    Kernel *kernel = new EdgeDetection(MASK_WIDTH);
    // sharpen kernel, MASK_WIDTH == 3
//    Kernel *kernel = new Kernel(MASK_WIDTH, "Sharpen", new float[MASK_WIDTH * MASK_WIDTH]{0, -1, 0, -1, 5, -1, 0, -1, 0});
    
    assert ( kernel->isFlippable() );

    std::cout << fn.substr(fn.size() - 9, 5) + '_' + kernel->getName() << '\n';

    // reading the image
    const Image_t *h_imgIn = Image_read(fn.c_str());

    const float *h_dataIn = Image_getData(h_imgIn);

    const int width = Image_getWidth(h_imgIn);
    const int height = Image_getHeight(h_imgIn);
    const int nChannels = Image_getNChannels(h_imgIn);
    const int depth = Image_getDepth(h_imgIn);

    Timer timer;

    // sequential version
    std::cout << ">>> SEQUENTIAL VERSION <<<\n";
    timer.start();
    float *h_dataOut_sequential = convolutionHost(h_dataIn, kernel, width, height, nChannels);
    timer.stop();
    const double elapsedSequential = timer.getElapsedTimeInMilliSec();
    std::cout << ">>> Time elapsed for sequential execution: " << elapsedSequential << " [ms].\n";

    Image_t *imgOut_sequential = Image_new(width, height, nChannels, depth, h_dataOut_sequential);
    Image_write(imgOut_sequential, (fn.substr(0, fn.size() - 4) + '_' + kernel->getName() + "_sequential.ppm").c_str());

    Image_delete(imgOut_sequential);

    // parallel version
    std::cout << "\n>>> PARALLEL VERSION <<<\n";
    timer.start();
    float *h_dataOut_parallel = convolutionKernelStub(h_dataIn, kernel, width, height, nChannels, elapsedSequential);
    timer.stop();
    const double elapsedParallel = timer.getElapsedTimeInMilliSec();
    std::cout << ">>> Time elapsed for parallel execution: " << elapsedParallel << " [ms].\n";

    std::cout << ">>> SpeedUp [Sequential / Parallel]: \tX " << elapsedSequential / elapsedParallel << '\n';

    Image_t *h_imgOut_parallel = Image_new(width, height, nChannels, depth, h_dataOut_parallel);
    Image_write(h_imgOut_parallel, (fn.substr(0, fn.size() - 4) + '_' + kernel->getName() + "_parallel.ppm").c_str());

    Image_delete(h_imgOut_parallel);

    // tiled version
    std::cout << "\n>>> TILED VERSION <<<\n";
    timer.start();
    float *h_dataOut_tiled = convolutionKernelTiledStub(h_dataIn, kernel, width, height, elapsedSequential);
    timer.stop();
    const double elapsedTiled = timer.getElapsedTimeInMilliSec();
    std::cout << ">>> Time elapsed for tiled execution: " << elapsedTiled << " [ms].\n";

    std::cout << ">>> SpeedUp [Sequential / Tiled]: \tX " << elapsedSequential / elapsedTiled << '\n';

    Image_t *h_imgOut_tiled = Image_new(width, height, nChannels, depth, h_dataOut_tiled);
    Image_write(h_imgOut_tiled, (fn.substr(0, fn.size() - 4) + '_' + kernel->getName() + "_tiled.ppm").c_str());

    Image_delete(h_imgOut_tiled);

    // tiled + constant version
    std::cout << "\n>>> TILED + CONSTANT VERSION <<<\n";
    timer.start();
    float *h_dataOut_tiledConstant = convolutionKernelTiledConstantStub(h_dataIn, kernel, width, height,
            elapsedSequential);
    timer.stop();
    const double elapsedTiledConstant = timer.getElapsedTimeInMilliSec();
    std::cout << ">>> Time elapsed for tiledConstant execution: " << elapsedTiledConstant << " [ms].\n";

    std::cout << ">>> SpeedUp [Sequential / TiledConstant]: \tX " << elapsedSequential / elapsedTiledConstant << '\n';

    Image_t *h_imgOut_tiledConstant = Image_new(width, height, nChannels, depth, h_dataOut_tiledConstant);
    Image_write(h_imgOut_tiledConstant,
            (fn.substr(0, fn.size() - 4) + '_' + kernel->getName() + "_tiledConstant.ppm").c_str());

    Image_delete(h_imgOut_tiledConstant);

    return 0;
}

__host__
float* convolutionHost(const float *in, const Kernel *kernel, const int w, const int h, const int nChannels) {
    auto *out = new float[w * h * nChannels];
    const float *mask = kernel->getMask();
    const int maskWidth = kernel->getMaskWidth();

    for (int i = 0; i < w * nChannels - nChannels + 1; i += 3) {
        for (int j = 0; j < h; ++j) {
            if (i < (w * nChannels - nChannels + 1) && j < h) {
                int idx = j * (w * nChannels) + i;
                float r = 0;
                float g = 0;
                float b = 0;

                for (int ii = -maskWidth / 2; ii < maskWidth / 2 + 1; ++ii) {
                    for (int jj = -maskWidth / 2; jj < maskWidth / 2 + 1; ++jj) {
                        int rowOffset = j + ii;
                        int colOffset = i + (jj * nChannels);
                        int idxOffset = rowOffset * (w * nChannels) + colOffset;

                        if (rowOffset > -1 && rowOffset < h && colOffset > -1 && colOffset < w * nChannels) {
                            r += in[idxOffset + 0] * mask[(ii + maskWidth / 2) * maskWidth + (jj + maskWidth / 2)];
                            g += in[idxOffset + 1] * mask[(ii + maskWidth / 2) * maskWidth + (jj + maskWidth / 2)];
                            b += in[idxOffset + 2] * mask[(ii + maskWidth / 2) * maskWidth + (jj + maskWidth / 2)];
                        }
                    }
                }
                out[idx + 0] = r;
                out[idx + 1] = g;
                out[idx + 2] = b;
            }
        }
    }
    return out;
}

__global__
void convolutionKernel(const float* __restrict__ in, float *out, const float* __restrict__ mask, const int maskWidth,
        const int w, const int h, const int nChannels) {

    int i = (blockIdx.x * blockDim.x + threadIdx.x) * nChannels;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if ( i < (w * nChannels) && j < h ) {
        int idx = j * (w * nChannels) + i;
        float r = 0;
        float g = 0;
        float b = 0;

        for (int ii = -maskWidth/2; ii <= maskWidth / 2; ++ii) {
            for (int jj = -maskWidth/2; jj <= maskWidth / 2; ++jj) {
                int rowOffset = j + ii;
                int colOffset = i + (jj * nChannels);
                int idxOffset = rowOffset * (w * nChannels) + colOffset;

                if (rowOffset > -1 && rowOffset < h && colOffset > -1 && colOffset < w * nChannels) {
                    r += in[idxOffset    ] * mask[(ii + maskWidth / 2) * maskWidth + (jj + maskWidth / 2)];
                    g += in[idxOffset + 1] * mask[(ii + maskWidth / 2) * maskWidth + (jj + maskWidth / 2)];
                    b += in[idxOffset + 2] * mask[(ii + maskWidth / 2) * maskWidth + (jj + maskWidth / 2)];
                }
            }
        }
        out[idx    ] = r;
        out[idx + 1] = g;
        out[idx + 2] = b;
    }
}

__host__
float* convolutionKernelStub(const float *h_dataIn, const Kernel *kernel, const int w, const int h,
        const int nChannels, double elapsedSequential) {

    const float *h_mask = kernel->getMask();
    const int maskWidth = kernel->getMaskWidth();

    // determining the memory size of the data
    int memSize = sizeof(float) * w * h * nChannels;
    int memSizeMask = sizeof(float) * maskWidth * maskWidth;

    // allocating memory in host
    float *h_dataOut = new float[w * h * nChannels];

    // allocating memory in device
    float *d_dataIn;
    float *d_dataOut;
    float *d_mask;
    cudaMalloc((void **) &d_dataIn, memSize);
    cudaMalloc((void **) &d_dataOut, memSize);
    cudaMalloc((void **) &d_mask, memSizeMask);

    // copying data from host to device
    cudaMemcpy(d_dataIn, h_dataIn, memSize, cudaMemcpyHostToDevice);
    cudaMemcpy(d_mask, h_mask, memSizeMask, cudaMemcpyHostToDevice);

    // kernel execution configuration parameters
    dim3 dimGrid(ceil( w / static_cast<float>(TILE_WIDTH) ),
                 ceil( h / static_cast<float>(TILE_WIDTH) ), 1);
    dim3 dimBlock( TILE_WIDTH, TILE_WIDTH, 1 );

    // calling kernel function
    Timer timer_parallel;
    timer_parallel.start();
    convolutionKernel<<< dimGrid, dimBlock >>>(d_dataIn, d_dataOut, d_mask, maskWidth, w, h, nChannels);
    cudaDeviceSynchronize();
    timer_parallel.stop();
    double elapsedParallelNoTransfer = timer_parallel.getElapsedTimeInMilliSec();
    std::cout << ">>> Time elapsed for parallel execution (NO TRANSFER): "
        << elapsedParallelNoTransfer << " [ms].\n";

    std::cout << ">>> SpeedUp [Sequential / Parallel (NO TRANSFER)]: \tX "
        << elapsedSequential / elapsedParallelNoTransfer << '\n';

    // copying data output from device to host
    cudaMemcpy(h_dataOut, d_dataOut, memSize, cudaMemcpyDeviceToHost);
    // freeing device memory
    cudaFree(d_dataIn);
    cudaFree(d_dataOut);

    return h_dataOut;
}

__global__
void convolutionKernelTiled(const float* __restrict__ in, float *out, const float* __restrict__ mask, const int width,
        const int height, const int width_grid, const int height_grid) {

    int by = blockIdx.y; int ty = threadIdx.y;
    int bx = blockIdx.x; int tx = threadIdx.x;

    int row_imageOut = by * TILE_WIDTH_OUT + (ty - MASK_WIDTH / 2);
    int col_imageOut = bx * TILE_WIDTH_OUT + (tx - MASK_WIDTH / 2);

    __shared__ float in_ds[TILE_WIDTH * TILE_WIDTH * CHANNELS];

    if ( -1 < row_imageOut && row_imageOut < height && -1 < col_imageOut && col_imageOut < width ) {
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS     ] = in[ (row_imageOut * width + col_imageOut) * CHANNELS     ];
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS + 1 ] = in[ (row_imageOut * width + col_imageOut) * CHANNELS + 1 ];
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS + 2 ] = in[ (row_imageOut * width + col_imageOut) * CHANNELS + 2 ];
    } else {
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS     ] = 0.0f;
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS + 1 ] = 0.0f;
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS + 2 ] = 0.0f;
    }

    __syncthreads();

    float r = 0.0f;
    float g = 0.0f;
    float b = 0.0f;

    if ( MASK_WIDTH / 2 <= ty && ty < TILE_WIDTH - MASK_WIDTH / 2 &&
         MASK_WIDTH / 2 <= tx && tx < TILE_WIDTH - MASK_WIDTH / 2 &&
         row_imageOut < height && col_imageOut < width ) {
        for (int i = -(MASK_WIDTH / 2); i <= MASK_WIDTH / 2; ++i) {
            for (int j = -(MASK_WIDTH / 2); j <= MASK_WIDTH / 2; ++j) {
                r += in_ds[ ( (ty + i) * TILE_WIDTH + (tx + j) ) * CHANNELS     ] * mask[ (i + MASK_WIDTH / 2) * MASK_WIDTH + (j + MASK_WIDTH / 2) ];
                g += in_ds[ ( (ty + i) * TILE_WIDTH + (tx + j) ) * CHANNELS + 1 ] * mask[ (i + MASK_WIDTH / 2) * MASK_WIDTH + (j + MASK_WIDTH / 2) ];
                b += in_ds[ ( (ty + i) * TILE_WIDTH + (tx + j) ) * CHANNELS + 2 ] * mask[ (i + MASK_WIDTH / 2) * MASK_WIDTH + (j + MASK_WIDTH / 2) ];
            }
        }
        out[ (row_imageOut * width + col_imageOut) * CHANNELS     ] = r;
        out[ (row_imageOut * width + col_imageOut) * CHANNELS + 1 ] = g;
        out[ (row_imageOut * width + col_imageOut) * CHANNELS + 2 ] = b;

    }
}

__host__
float* convolutionKernelTiledStub(const float *h_dataIn, const Kernel *kernel, const int w, const int h,
        double elapsedSequential) {

    const float *h_mask = kernel->getMask();

    // determining the memory size of the data
    int memSize = sizeof(float) * w * h * CHANNELS;
    int memSizeMask = sizeof(float) * MASK_WIDTH * MASK_WIDTH;

    // allocating memory in host
    float *h_dataOut = new float[w * h * CHANNELS];

    // allocating memory in device
    float *d_dataIn;
    float *d_dataOut;
    float *d_mask;
    cudaMalloc((void **) &d_dataIn, memSize);
    cudaMalloc((void **) &d_dataOut, memSize);
    cudaMalloc((void **) &d_mask, memSizeMask);

    // copying data from host to device
    cudaMemcpy(d_dataIn, h_dataIn, memSize, cudaMemcpyHostToDevice);
    cudaMemcpy(d_mask, h_mask, memSizeMask, cudaMemcpyHostToDevice);

    // kernel execution configuration parameters for tiling version
    int nBlocks_x = ceil( w / static_cast<float>(TILE_WIDTH_OUT) );
    int nBlocks_y = ceil( h / static_cast<float>(TILE_WIDTH_OUT) );
    dim3 dimGrid( nBlocks_x, nBlocks_y, 1);
    dim3 dimBlock( TILE_WIDTH, TILE_WIDTH, 1 );

    int width_grid = nBlocks_x * TILE_WIDTH;
    int height_grid = nBlocks_y * TILE_WIDTH;

    // calling kernel tiled function
    Timer timer_tiled;
    timer_tiled.start();
    convolutionKernelTiled<<< dimGrid, dimBlock >>>(d_dataIn, d_dataOut, d_mask, w, h, width_grid, height_grid);
    cudaDeviceSynchronize();
    timer_tiled.stop();

    double elapsedTiledNoTransfer = timer_tiled.getElapsedTimeInMilliSec();
    std::cout << ">>> Time elapsed for tiled execution (NO TRANSFER): "
        << elapsedTiledNoTransfer << " [ms].\n";

    std::cout << ">>> SpeedUp [Sequential / Tiled (NO TRANSFER)]: \tX "
              << elapsedSequential / elapsedTiledNoTransfer << '\n';
    // copying data output from device to host
    cudaMemcpy(h_dataOut, d_dataOut, memSize, cudaMemcpyDeviceToHost);

    // freeing device memory
    cudaFree(d_dataIn);
    cudaFree(d_dataOut);

    return h_dataOut;
}

__global__
void convolutionKernelTiledConstant(const float* __restrict__ in, float *out, const int width, const int height,
        const int width_grid, const int height_grid) {

    int by = blockIdx.y; int ty = threadIdx.y;
    int bx = blockIdx.x; int tx = threadIdx.x;

    int row_imageOut = by * TILE_WIDTH_OUT + (ty - MASK_WIDTH / 2);
    int col_imageOut = bx * TILE_WIDTH_OUT + (tx - MASK_WIDTH / 2);

    __shared__ float in_ds[TILE_WIDTH * TILE_WIDTH * CHANNELS];

    if ( -1 < row_imageOut && row_imageOut < height && -1 < col_imageOut && col_imageOut < width ) {
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS     ] = in[ (row_imageOut * width + col_imageOut) * CHANNELS     ];
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS + 1 ] = in[ (row_imageOut * width + col_imageOut) * CHANNELS + 1 ];
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS + 2 ] = in[ (row_imageOut * width + col_imageOut) * CHANNELS + 2 ];
    } else {
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS     ] = 0.0f;
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS + 1 ] = 0.0f;
        in_ds[ (ty * TILE_WIDTH + tx) * CHANNELS + 2 ] = 0.0f;
    }

    __syncthreads();

    float r = 0.0f;
    float g = 0.0f;
    float b = 0.0f;

    if ( MASK_WIDTH / 2 <= ty && ty < TILE_WIDTH - MASK_WIDTH / 2 &&
         MASK_WIDTH / 2 <= tx && tx < TILE_WIDTH - MASK_WIDTH / 2 &&
         row_imageOut < height && col_imageOut < width ) {
        for (int i = -(MASK_WIDTH / 2); i <= MASK_WIDTH / 2; ++i) {
            for (int j = -(MASK_WIDTH / 2); j <= MASK_WIDTH / 2; ++j) {
                r += in_ds[ ( (ty + i) * TILE_WIDTH + (tx + j) ) * CHANNELS     ] * MASK[ (i + MASK_WIDTH / 2) * MASK_WIDTH + (j + MASK_WIDTH / 2) ];
                g += in_ds[ ( (ty + i) * TILE_WIDTH + (tx + j) ) * CHANNELS + 1 ] * MASK[ (i + MASK_WIDTH / 2) * MASK_WIDTH + (j + MASK_WIDTH / 2) ];
                b += in_ds[ ( (ty + i) * TILE_WIDTH + (tx + j) ) * CHANNELS + 2 ] * MASK[ (i + MASK_WIDTH / 2) * MASK_WIDTH + (j + MASK_WIDTH / 2) ];
            }
        }
        out[ (row_imageOut * width + col_imageOut) * CHANNELS     ] = r;
        out[ (row_imageOut * width + col_imageOut) * CHANNELS + 1 ] = g;
        out[ (row_imageOut * width + col_imageOut) * CHANNELS + 2 ] = b;
    }
}

__host__
float* convolutionKernelTiledConstantStub(const float *h_dataIn, const Kernel *kernel, const int w, const int h,
                                  double elapsedSequential) {

    const float *h_mask = kernel->getMask();

    // determining the memory size of the data
    int memSize = sizeof(float) * w * h * CHANNELS;

    // allocating memory in host
    float *h_dataOut = new float[w * h * CHANNELS];

    // allocating memory in device
    float *d_dataIn;
    float *d_dataOut;
    cudaMalloc((void **) &d_dataIn, memSize);
    cudaMalloc((void **) &d_dataOut, memSize);

    // copying constant memory in device
//    cudaMemcpyToSymbol("MASK", h_mask, MASK_WIDTH * MASK_WIDTH * sizeof(float), 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(MASK, h_mask, MASK_WIDTH * MASK_WIDTH * sizeof(float));

    // copying data from host to device
    cudaMemcpy(d_dataIn, h_dataIn, memSize, cudaMemcpyHostToDevice);

    // kernel execution configuration parameters for tiling version
    int nBlocks_x = ceil( (w + (MASK_WIDTH - 1)) / static_cast<float>(TILE_WIDTH_OUT) );
    int nBlocks_y = ceil( (h + (MASK_WIDTH - 1)) / static_cast<float>(TILE_WIDTH_OUT) );
    dim3 dimGrid( nBlocks_x, nBlocks_y, 1);
    dim3 dimBlock( TILE_WIDTH, TILE_WIDTH, 1 );

    int width_grid = nBlocks_x * TILE_WIDTH;
    int height_grid = nBlocks_y * TILE_WIDTH;

    // calling kernel tiled function
    Timer timer_tiled;
    timer_tiled.start();
    convolutionKernelTiledConstant<<< dimGrid, dimBlock >>>(d_dataIn, d_dataOut, w, h, width_grid, height_grid);
    cudaDeviceSynchronize();
    timer_tiled.stop();

    double elapsedTiledConstantNoTransfer = timer_tiled.getElapsedTimeInMilliSec();
    std::cout << ">>> Time elapsed for tiledConstant execution (NO TRANSFER): "
              << elapsedTiledConstantNoTransfer << " [ms].\n";

    std::cout << ">>> SpeedUp [Sequential / TiledConstant (NO TRANSFER)]: \tX "
              << elapsedSequential / elapsedTiledConstantNoTransfer << '\n';
    // copying data output from device to host
    cudaMemcpy(h_dataOut, d_dataOut, memSize, cudaMemcpyDeviceToHost);

    // freeing device memory
    cudaFree(d_dataIn);
    cudaFree(d_dataOut);

    return h_dataOut;
}
