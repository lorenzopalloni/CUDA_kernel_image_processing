//
// Created by lore on 4/1/19.
//

#ifndef KERNEL_H
#define KERNEL_H

#include <iostream>
#include <cmath>

constexpr float pi = 3.141693;

class Kernel {
protected:
    int maskWidth;
    const char *name;
    float *mask = nullptr;
public:
    explicit Kernel(int p_maskWidth=0, const char *p_name=""):
            maskWidth{p_maskWidth}, name{p_name} { }
    Kernel(int p_maskWidth, const char *p_name, float *p_mask):
            Kernel(p_maskWidth, p_name) { mask = p_mask; }
    Kernel(int p_maskWidth, float *mask): Kernel(p_maskWidth, "", mask) { }

    virtual ~Kernel();
    virtual int getMaskWidth() const { return maskWidth; }
    virtual const char *getName() const { return name; }
    virtual float *getMask() const { return mask; }
    virtual void print() const;
    virtual bool isFlippable() const;

};

std::ostream& operator<< (std::ostream &out, Kernel *kernel);

class BoxBlur: public Kernel {
public:
    explicit BoxBlur(int p_maskWidth=3, const char *p_name="BoxBlur");
};

class GaussianBlur: public Kernel {
private:
    float stddev;
public:
    explicit GaussianBlur(float p_stddev=1, int p_maskWidth=3, const char *p_name="GaussianBlur");
    float getStddev() { return stddev; }
};

class EdgeDetection: public Kernel {
public:
    explicit EdgeDetection(int p_maskWidth=3, const char *p_name="EdgeDetection");
};



#endif //KERNEL_H
