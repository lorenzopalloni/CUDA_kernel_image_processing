#include <cuda.h>
#include <iostream>

int main() {
    
    int dev_count;
    cudaGetDeviceCount(&dev_count);
            
    cudaDeviceProp dev_prop;
    for (int i = 0; i < dev_count; ++i) {
        cudaGetDeviceProperties(&dev_prop, i);
    }
    std::cout << "name: " << dev_prop.name << '\n';
    std::cout << "totalGlobalMem: " << dev_prop.totalGlobalMem << '\n';
    std::cout << "sharedMemPerBlock: " << dev_prop.sharedMemPerBlock << '\n';
    std::cout << "regsPerBlock: " << dev_prop.regsPerBlock << '\n';
    std::cout << "warpSize: " << dev_prop.warpSize << '\n';
    std::cout << "memPitch: " << dev_prop.memPitch << '\n';
    std::cout << "maxThreadsPerBlock: " << dev_prop.maxThreadsPerBlock << '\n';
    std::cout << "maxThreadsDim: ( " << dev_prop.maxThreadsDim[0] << ", " << 
        dev_prop.maxThreadsDim[1] << ", " << dev_prop.maxThreadsDim[2] << " )\n";
    std::cout << "maxGridSize: ( " << dev_prop.maxGridSize[0] << ", " <<
        dev_prop.maxGridSize[1] << ", " << dev_prop.maxGridSize[2] << " )\n";
    std::cout << "totalConstMem: " << dev_prop.totalConstMem << '\n';
    
    std::cout << "major: " << dev_prop.major << '\n';
    std::cout << "minor: " << dev_prop.minor << '\n';
    std::cout << "clockRate: " << dev_prop.clockRate << '\n';
    std::cout << "textureAlignment: " << dev_prop.textureAlignment << '\n';
    std::cout << "deviceOverlap: " << dev_prop.deviceOverlap << '\n';
    std::cout << "multiProcessorCount: " << dev_prop.multiProcessorCount << '\n';
    std::cout << "kernelExecTimeoutEnabled: " << dev_prop.kernelExecTimeoutEnabled << '\n';
    std::cout << "integrated: " << dev_prop.integrated << '\n';
    std::cout << "canMapHostMemory: " << dev_prop.canMapHostMemory << '\n';
    std::cout << "computeMode: " << dev_prop.computeMode << '\n';
    std::cout << "concurrentKernels: " << dev_prop.concurrentKernels << '\n';
    std::cout << "ECCEnabled: " << dev_prop.ECCEnabled << '\n';
    std::cout << "pciBusID: " << dev_prop.pciBusID << '\n';
    std::cout << "tccDriver: " << dev_prop.tccDriver << '\n';
    
    return 0;
}
